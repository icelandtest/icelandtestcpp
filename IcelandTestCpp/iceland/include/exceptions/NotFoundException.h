#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class NotFoundException : public std::exception {
        std::string msg;
    public:
        NotFoundException() noexcept { initializeMsg(""); }
        explicit NotFoundException(char const * msg) noexcept { initializeMsg(msg); }
        explicit NotFoundException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("NotFoundException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, NotFoundException & exc) {
        return stream << exc.what();
    }

}

