#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class OutOfBoundsException : public std::exception {
        std::string msg;
    public:
        OutOfBoundsException() noexcept { initializeMsg(""); }
        explicit OutOfBoundsException(char const * msg) noexcept { initializeMsg(msg); }
        explicit OutOfBoundsException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("OutOfBoundsException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, OutOfBoundsException & exc) {
        return stream << exc.what();
    }

}

