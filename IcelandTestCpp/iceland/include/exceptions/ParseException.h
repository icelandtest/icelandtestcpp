#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class ParseException : public std::exception {
        std::string msg;
    public:
        ParseException() noexcept { initializeMsg(""); }
        explicit ParseException(char const * msg) noexcept { initializeMsg(msg); }
        explicit ParseException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("ParseException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, ParseException & exc) {
        return stream << exc.what();
    }

}

