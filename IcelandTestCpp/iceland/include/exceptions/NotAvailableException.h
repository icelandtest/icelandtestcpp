#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class NotAvailableException : public std::exception {
        std::string msg;
    public:
        NotAvailableException() noexcept { initializeMsg(""); }
        explicit NotAvailableException(char const * msg) noexcept { initializeMsg(msg); }
        explicit NotAvailableException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("NotAvailableException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, NotAvailableException & exc) {
        return stream << exc.what();
    }
}
#pragma once

