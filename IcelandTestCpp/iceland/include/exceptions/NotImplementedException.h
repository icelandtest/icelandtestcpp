#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class NotImplementedException : public std::exception {
        std::string msg;
    public:
        NotImplementedException() noexcept { initializeMsg(""); }
        explicit NotImplementedException(char const * msg) noexcept { initializeMsg(msg); }
        explicit NotImplementedException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("NotImplementedException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, NotImplementedException & exc) {
        return stream << exc.what();
    }

}

