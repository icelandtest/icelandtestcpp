#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class DuplicateException : public std::exception {
        std::string msg;
    public:
        DuplicateException() noexcept { initializeMsg(""); }
        explicit DuplicateException(char const * msg) noexcept { initializeMsg(msg); }
        explicit DuplicateException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("DuplicateException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, DuplicateException & exc) {
        return stream << exc.what();
    }

}

