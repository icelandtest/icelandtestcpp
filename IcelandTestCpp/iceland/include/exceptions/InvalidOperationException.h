#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class InvalidOperationException : public std::exception {
        std::string msg;
    public:
        InvalidOperationException() noexcept { initializeMsg(""); }
        explicit InvalidOperationException(char const * msg) noexcept { initializeMsg(msg); }
        explicit InvalidOperationException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("InvalidOperationException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, InvalidOperationException & exc) {
        return stream << exc.what();
    }

}

