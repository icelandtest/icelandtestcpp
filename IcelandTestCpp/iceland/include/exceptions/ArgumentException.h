#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class ArgumentException : public std::exception {
        std::string msg;
    public:
        ArgumentException() noexcept { initializeMsg(""); }
        explicit ArgumentException(char const * msg) noexcept { initializeMsg(msg); }
        explicit ArgumentException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("ArgumentException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, ArgumentException & exc) {
        return stream << exc.what();
    }

}

