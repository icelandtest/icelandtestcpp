#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class DataWasModifiedException : public std::exception {
        std::string msg;
    public:
        DataWasModifiedException() noexcept { initializeMsg(""); }
        explicit DataWasModifiedException(char const * msg) noexcept { initializeMsg(msg); }
        explicit DataWasModifiedException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("DataWasModifiedException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, DataWasModifiedException & exc) {
        return stream << exc.what();
    }

}

