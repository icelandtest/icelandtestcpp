#pragma once

#include <exception>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class StateException : public std::exception {
        std::string msg;
    public:
        StateException() noexcept { initializeMsg(""); }
        explicit StateException(char const * msg) noexcept { initializeMsg(msg); }
        explicit StateException(std::string msg) noexcept { initializeMsg(msg.c_str()); }
        const char* what() const noexcept override { return msg.c_str(); }
    private:
        void initializeMsg(char const * msgFromCtor) { msg = std::string("StateException(") + msgFromCtor + ')';
        }
    };

    inline std::ostream & operator << (std::ostream & stream, StateException & exc) {
        return stream << exc.what();
    }

}

