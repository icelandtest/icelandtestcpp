#pragma once

#include <exception>
#include <optional>
#include <string>
#include <iostream>

namespace iceland::exceptions {

    class IcelandMatcherFailedException : public std::exception {
        std::optional<std::string> msg;
    public:
        IcelandMatcherFailedException() noexcept = default;
        explicit IcelandMatcherFailedException(char const * msg) noexcept : msg(std::string(msg)) { }
        explicit IcelandMatcherFailedException(std::string msg) noexcept : msg(msg) { }
        const char* what() const noexcept override {
            if (msg.has_value())
                return msg->c_str();
            return nullptr;
        }
    };

    inline std::ostream & operator << (std::ostream & stream, IcelandMatcherFailedException & exc) {
        return stream << "IcelandMatcherFailedException: " << exc.what();
    }
}

