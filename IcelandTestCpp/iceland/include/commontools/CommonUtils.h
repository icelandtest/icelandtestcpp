#ifndef ICELAND_COMMONUTILS_H
#define ICELAND_COMMONUTILS_H

#include <type_traits>
#include <functional>
#include <optional>
#include <thread>

namespace iceland::commontools {

    template <typename TObtained, typename TExpected>
    struct typeisCompatibleTo_struct {
        static constexpr bool value =
                std::is_fundamental_v<TExpected>
                        ? std::is_same_v<TObtained, TExpected> // fundamental types must be identical
                        : std::is_convertible_v<TObtained, TExpected>; // non fundamental types must be convertible
    };
    template <typename TObtained, typename TExpected>
    constexpr bool typeIsCompatibleTo = typeisCompatibleTo_struct<TObtained, TExpected>::value;

    template <typename TObtained, typename TExpected>
    constexpr bool typesAreIdentical = std::is_same_v<TObtained, TExpected>;

    template <typename TExpected, typename TObj>
    inline bool objIsOfCompatibleTypeTo(TObj const &obj) { return typeIsCompatibleTo<TObj, TExpected>; }

    template <typename TExpected, typename TObj>
    inline bool objIsOfExactType(TObj const &obj) { return typesAreIdentical<TObj, TExpected>; }

}

#endif //ICELAND_COMMONUTILS_H
