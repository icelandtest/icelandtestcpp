#ifndef ICELAND_INDEXABLEUTILS_H
#define ICELAND_INDEXABLEUTILS_H

#include <algorithm>
#include <iostream>
#include <commontools/IdxDifferenceFlags.h>

namespace iceland::commontools {

    template <typename TLhsIndexable, typename TRhsIndexable>
    inline decltype(auto) idxOfFirstDifferenceBetween(TLhsIndexable const &lhs, TRhsIndexable const &rhs) {
        using SizeType = decltype(lhs.size());
        SizeType lhsSize = lhs.size();
        auto rhsSize = static_cast<SizeType>(rhs.size()); // we need this cast if the two indexables have different size-types
        auto idxUpperBound = std::min(lhsSize, rhsSize);
        for (SizeType idx = 0; idx < idxUpperBound; ++idx) {
            if (!(lhs[idx] == rhs[idx])) // we don't use != here but a negation of ==, because we only require implementation of the == operator on the stored elements
                return idx;
        }
        if (lhsSize == rhsSize) return static_cast<SizeType>(IdxDifferenceFlags::NoDifference);
        if (lhsSize > rhsSize) return static_cast<SizeType>(IdxDifferenceFlags::LhsIsLonger);
        return static_cast<SizeType>(IdxDifferenceFlags::RhsIsLonger);
    }

}

#endif //ICELAND_INDEXABLEUTILS_H
