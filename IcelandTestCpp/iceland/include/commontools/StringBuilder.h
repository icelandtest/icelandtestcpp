#pragma once

#include <string>

namespace iceland::commontools {

    class StringBuilder {
        std::stringstream stringStream;
    public:
        static StringBuilder str() { return StringBuilder(); }

        template<typename TElement>
        StringBuilder & operator + (TElement const& element) {
            stringStream << element;
            return *this;
        }

        operator std::string() { return stringStream.str(); }
    };

}