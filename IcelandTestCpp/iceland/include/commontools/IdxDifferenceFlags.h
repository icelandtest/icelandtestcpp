#ifndef ICELAND_IDXDIFFERENCEFLAGS_H
#define ICELAND_IDXDIFFERENCEFLAGS_H

namespace iceland::commontools {

    enum class IdxDifferenceFlags {
        NoDifference = -1,
        LhsIsLonger = -2,
        RhsIsLonger = -3
    };

}

#endif //ICELAND_IDXDIFFERENCEFLAGS_H
