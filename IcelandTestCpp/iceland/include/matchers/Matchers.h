#pragma once

#include <iostream>
#include <exceptions/IcelandMatcherFailedException.h>
#include <matchers/MatcherBuilder.h>

namespace iceland::matchers {
    using namespace iceland::exceptions;

    template <typename TObj>
    void typeAsSeenByTheCompiler(TObj obj); // just for debugging purposes

    template <typename TObj>
    void typeAsSeenByTheCompiler(); // just for debugging purposes

    inline void fail(const char* msg) {
        std::stringstream errorMsg;
        errorMsg << "failed: " << msg;
        throw IcelandMatcherFailedException(errorMsg.str());
    }
    inline void fail(std::function<const char* ()> msg) { fail(msg()); }

    template <typename TObtained>
    MatcherBuilder<TObtained> match(TObtained const& obtained, std::optional<std::string> additionalMsgOnFail = {}) {
        return MatcherBuilder([](std::string const& errorMsg) { return errorMsg; }, obtained, additionalMsgOnFail);
    }

    template <typename TObtained>
    MatcherBuilder<TObtained> expectThat(TObtained const& obtained, std::optional<std::string> additionalMsgOnFail = {}) {
        return MatcherBuilder(
                [](std::string const& errorMsg) -> std::string { throw IcelandMatcherFailedException(errorMsg); },
                obtained, additionalMsgOnFail);
    }

    // support for full-custom matchers
    class CustomMatcher {
    public:
        virtual std::optional<std::string> operator()() const = 0;
    };

    inline std::optional<std::string> customMatch(CustomMatcher const& customMatcher) {
        std::optional<std::string> errorMsg;
        try {
            errorMsg = customMatcher();
        } catch (...) {
            return"custom matcher malfunction: unknown exception thrown on execution";
        }
        return errorMsg;
    }

    inline void customExpectThat(CustomMatcher const& customMatcher) {
        std::optional<std::string> errorMsg;
        try {
            errorMsg = customMatcher();
        } catch (...) {
            throw IcelandMatcherFailedException("custom matcher malfunction: unknown exception thrown on execution");
        }
        if (errorMsg.has_value())
            throw IcelandMatcherFailedException(errorMsg.value());
    }

}
