#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <cmath>
#include <unordered_set>
#include <commontools/CommonUtils.h>
#include <commontools/IndexableUtils.h>
#include <exceptions/IcelandMatcherFailedException.h>

namespace iceland::matchers {
    using namespace iceland::exceptions;
    using namespace iceland::commontools;

    template<typename TObtained>
    class MatcherBuilder {
    private:
        std::function<std::string(std::string const&)> doThatOnFail;
        TObtained const& obtained;
        std::optional<std::string> additionalMsgOnFail;
    public:
        MatcherBuilder(
                std::function<std::string(std::string const&)> doThatOnFail,
                TObtained const& obtained,
                std::optional<std::string> additionalMsgOnFail) :
                doThatOnFail(std::move(doThatOnFail)), obtained(obtained), additionalMsgOnFail(std::move(additionalMsgOnFail)) { }

        std::optional<std::string> isTrue() {
            if (obtained == true)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isTrue failed";
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isFalse() {
            if (obtained == false)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isFalse failed";
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isEqualTo(TExpected const& expected) {
            if (obtained == expected)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isEqualTo failed: values are not equal" << std::endl <<
            "expected: " << expected << std::endl <<
            "but obtained: " << obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
           return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isNotEqualTo(TExpected const& expected) {
            if (!(obtained == expected)) // we always use the == operator for comparison
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNotEqualTo failed: values are equal" << std::endl <<
                     expected;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        // Note: all comparison-matchers solely use the "<" operator and do not rely on any other operator to be defined
        template <typename TExpected>
        std::optional<std::string> isLessThan(TExpected const& expected) {
            if (obtained < expected)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isLessThan failed" << std::endl <<
                     "obtained: " << obtained << std::endl <<
                     "is not less than: " << expected;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isGreaterThan(TExpected const& expected) {
            if (expected < obtained)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isGreaterThan failed" << std::endl <<
                     "obtained: " << obtained << std::endl <<
                     "is not greater than: " << expected;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isLessThanOrEqualTo(TExpected const& expected) {
            if (!(expected < obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isLessThanOrEqualTo failed" << std::endl <<
                     "obtained: " << obtained << std::endl <<
                     "is not less or equal than: " << expected;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isGreaterThanOrEqualTo(TExpected const& expected) {
            if (!(obtained < expected))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isGreaterThanOrEqualTo failed" << std::endl <<
                     "obtained: " << obtained << std::endl <<
                     "is not greater or equal than: " << expected;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isNullPtr() {
            if (obtained == nullptr)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNullPtr failed: value is not a null-pointer" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isNotNullPtr() {
            if (obtained != nullptr)
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNotNullPtr failed: value is a null-pointer";
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isNaN() {
            if (std::isnan(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNaN failed: value is not NaN" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isNotNaN() {
            if (!std::isnan(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNotNaN failed: value is NaN";
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isInf() {
            if (std::isinf(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isInf failed: value is not infinite" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        std::optional<std::string> isNotInf() {
            if (!std::isinf(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNotInf failed: value is infinite";
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        // --------------------------------------------------------------------------------
        template<typename TExpected>
        class IsCloserToMatcherBuilder {
        public:
            MatcherBuilder<TObtained> const & outerMatcherBuilder;
            TExpected const & expected;
            bool diffShouldBeInsideEpsilon;

            IsCloserToMatcherBuilder(MatcherBuilder<TObtained> const & outerMatcherBuilder, TExpected const & expected,
                                     bool diffShouldBeInsideEpsilon) // support for isCloserTo/isNotCloserTo
                    : outerMatcherBuilder(outerMatcherBuilder), expected(expected),
                    diffShouldBeInsideEpsilon(diffShouldBeInsideEpsilon) {}

            template<typename TEpsilon>
            std::optional<std::string> than(TEpsilon const & epsilon) {
                auto absDiff = std::abs(outerMatcherBuilder.obtained - expected);
                auto absEpsilon = std::abs(epsilon);
                auto diffIsInsideEpsilon = absDiff < absEpsilon;
                if (diffShouldBeInsideEpsilon && diffIsInsideEpsilon)
                    return {};
                if (!diffShouldBeInsideEpsilon && !diffIsInsideEpsilon)
                    return {};
                auto errorMsg = std::stringstream();
                auto matcherName = diffShouldBeInsideEpsilon ? "isCloserTo" : "isNotCloserTo";
                errorMsg << matcherName << " than " << absEpsilon << " failed" << std::endl <<
                         "the absolute difference between expected: " << expected << std::endl <<
                         "and obtained: " << outerMatcherBuilder.obtained << std::endl <<
                         "is: " << absDiff;
                if (outerMatcherBuilder.additionalMsgOnFail.has_value())
                    errorMsg << std::endl << outerMatcherBuilder.additionalMsgOnFail.value();
                return std::make_optional(outerMatcherBuilder.doThatOnFail(errorMsg.str()));
            }
        };
        // --------------------------------------------------------------------------------

        template <typename TExpected>
        IsCloserToMatcherBuilder<TExpected> isCloserTo(TExpected const& expected) {
            return MatcherBuilder<TObtained>::IsCloserToMatcherBuilder<TExpected>(*this, expected, true);
        }

        template <typename TExpected>
        IsCloserToMatcherBuilder<TExpected> isNotCloserTo(TExpected const& expected) {
            return MatcherBuilder<TObtained>::IsCloserToMatcherBuilder<TExpected>(*this, expected, false);
        }

        template<typename TException>
        std::optional<std::string> throws(std::function<bool(TException const&)> isExceptionOk = [](TException const& exc) { return true; }) {
            try {
                obtained(); // must be any kind of callable, otherwise this would make no sense :-)
            } catch(TException const & exc) {
                if (isExceptionOk(exc))
                    return {};
                auto errorMsg = std::stringstream();
                errorMsg << "throws failed - obtained compatible exception with what() being: ";
                if constexpr (iceland::commontools::typeIsCompatibleTo<TException, std::exception>)
                    errorMsg << exc.what();
                else
                    errorMsg << "of type that is not derived from std::exception";
                errorMsg << std::endl << "was not considered ok";
                if (additionalMsgOnFail.has_value())
                    errorMsg << std::endl << additionalMsgOnFail.value();
                return std::make_optional(doThatOnFail(errorMsg.str()));
            } catch(...) {
                auto errorMsg = std::stringstream();
                errorMsg << "throws failed - obtained exception was of unexpected type";
                if (additionalMsgOnFail.has_value())
                    errorMsg << std::endl << additionalMsgOnFail.value();
                return std::make_optional(doThatOnFail(errorMsg.str()));
            }
            auto errorMsg = std::stringstream();
            errorMsg << "throws failed - no exception was thrown at all";
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isOfExactType() {
            if (objIsOfExactType<TExpected>(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isOfExactType failed: value is not of expected type" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isNotOfExactType() {
            if (!objIsOfExactType<TExpected>(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNotOfExactType failed: value is of non-expected type" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isOfCompatibleTypeTo() {
            if (objIsOfCompatibleTypeTo<TExpected>(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isOfCompatibleTypeTo failed: value is not of compatible type" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> isNotOfCompatibleTypeTo() {
            if (!objIsOfCompatibleTypeTo<TExpected>(obtained))
                return {};
            auto errorMsg = std::stringstream();
            errorMsg << "isNotOfCompatibleTypeTo failed: value is of non-expected compatible type" << std::endl <<
                     obtained;
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();
            return std::make_optional(doThatOnFail(errorMsg.str()));
        }

        template <typename TExpected>
        std::optional<std::string> hasElements(TExpected const &expectedIndexable) {
            auto idxOfFirstDifference = idxOfFirstDifferenceBetween(obtained, expectedIndexable);
            return evaluateResultOfIdxOfFirstDifference(idxOfFirstDifference);
        }

    private:
        template <typename TIdx>
        std::optional<std::string> evaluateResultOfIdxOfFirstDifference(TIdx idxOfFirstDifference) {
            if (idxOfFirstDifference == (int)IdxDifferenceFlags::NoDifference)
                return {};

            auto errorMsg = std::stringstream();
            errorMsg << "hasElements failed: ";

            if (idxOfFirstDifference == (int)IdxDifferenceFlags::LhsIsLonger)
                errorMsg << "obtained indexable is longer than expected indexable";
            else {
                if (idxOfFirstDifference == (int)IdxDifferenceFlags::RhsIsLonger)
                    errorMsg << "obtained indexable is shorter than expected indexable";
                else
                    errorMsg << "first difference found at index: " << std::to_string(idxOfFirstDifference);
            }
            if (additionalMsgOnFail.has_value())
                errorMsg << std::endl << additionalMsgOnFail.value();

            return std::make_optional(doThatOnFail(errorMsg.str()));
        }
    };
}
