#include <gtest/gtest.h>
#include <matchers/Matchers.h>

#include <exceptions/ArgumentException.h>
#include <exceptions/DataWasModifiedException.h>
#include <exceptions/DuplicateException.h>
#include <exceptions/IcelandMatcherFailedException.h>
#include <exceptions/InvalidOperationException.h>
#include <exceptions/NotAvailableException.h>
#include <exceptions/NotFoundException.h>
#include <exceptions/NotImplementedException.h>
#include <exceptions/OutOfBoundsException.h>
#include <exceptions/ParseException.h>
#include <exceptions/StateException.h>


namespace icelandexamples {

    namespace iceT = iceland::matchers;
    using namespace iceland::exceptions;

    TEST(icelandExamples, explainDifferentExceptions) {
        // there are more exceptions that come together with the matchers than the matchers would need...
        // ... the reason is, that when writing clean code, we need more than the few standard exceptions
        // that come with C++...
        // ... and TDD also has a lot to do with writing clean code :-)
        // ... so a few exceptions for frequent cases are included in the package...

        try {
            // this one is used frequently for TDD, when writing method-stubs that contain no meaningful
            // implementation yet
            throw NotImplementedException(); // we could also pass a msg to the constructor - explanation follows below...
        } catch (NotImplementedException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("NotImplementedException()");
        }

        try {
            // this one is used by the matchers and has to be used when writing new matchers
            throw IcelandMatcherFailedException("oops");
        } catch (IcelandMatcherFailedException const& exc) {
            // this is the only exception from that package that passes on the given message
            // without any decoration, as we need it to provide good fail-information
            iceT::expectThat(exc.what()).isEqualTo(std::string("oops"));
        }

        try {
            // this one is used to tell about problems with arguments passed to a function
            throw ArgumentException("whatever");
        } catch (ArgumentException const& exc) {
            // that's what all the exceptions except IcelandMatcherFailedException do with the message
            // that we pass during construction: they enclose the msg in their typename
            iceT::expectThat(std::string(exc.what())).isEqualTo("ArgumentException(whatever)");
        }

        try {
            // this one is used to alert about unexpected modifications of data
            throw DataWasModifiedException();
        } catch (DataWasModifiedException const& exc) {
            // that's what all the exceptions except IcelandMatcherFailedException do, if no message
            // is passed during construction: the typename is still there, but nothing is inside the braces
            iceT::expectThat(std::string(exc.what())).isEqualTo("DataWasModifiedException()");
        }

        try {
            // this one is used to alert about unexpected duplicates, e.g. when trying to insert an entry into
            // a dictionary twice
            throw DuplicateException();
        } catch (DuplicateException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("DuplicateException()");
        }

        try {
            // this one is used to alert about an operation request that is currently not valid for any reason,
            // e.g. when trying to set a write-once property twice
            throw InvalidOperationException();
        } catch (InvalidOperationException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("InvalidOperationException()");
        }

        try {
            // this one is used to alert about an unavailable object, e.g. when trying to obtain a
            // resource which is currently taken by somebody else
            throw NotAvailableException();
        } catch (NotAvailableException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("NotAvailableException()");
        }

        try {
            // this one is used to alert about not finding a desired object, e.g. when requesting
            // an entry for a dictionary with a key that isn't there
            throw NotFoundException();
        } catch (NotFoundException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("NotFoundException()");
        }

        try {
            // this one is used to alert about anything that goes out of bounds of a container, e.g.
            // an index that is too high
            throw OutOfBoundsException();
        } catch (OutOfBoundsException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("OutOfBoundsException()");
        }

        try {
            // this one is used to alert about problems when parsing something, e.g. an invalid number-string
            throw ParseException();
        } catch (ParseException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("ParseException()");
        }

        try {
            // this one is used to alert about problems with the state of an object, e.g. when trying
            // to connect to a remote socket, but the address is not set yet
            throw StateException();
        } catch (StateException const& exc) {
            iceT::expectThat(std::string(exc.what())).isEqualTo("StateException()");
        }
    }

}