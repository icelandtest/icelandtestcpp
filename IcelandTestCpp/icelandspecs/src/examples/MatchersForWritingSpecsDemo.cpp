// In fact you can use any test-framework with the matchers as they do not
// interfere with the test-runner at all. The matchers are just a collection
// of headers that you include in your project and they are used to express
// expectations in your testcases.

#include <gtest/gtest.h> // we use googletest here

#include <matchers/Matchers.h> // this is the header for the matchers
#include <exceptions/NotImplementedException.h> // this is the recommended exception for "not implemented" when doing TDD
#include <commontools/StringBuilder.h>

#include <array>
#include <vector>

namespace icelandexamples {

    namespace iceT = iceland::matchers; // I personally like this namespace-alias :-)
    using namespace iceland::exceptions; // I personally like this one too...
    using iceland::commontools::StringBuilder;

    TEST(icelandExamples, weCanUseMatchOrExpectThatWithOurMatchers) {
        // using expectThat would throw on failures
        iceT::expectThat(42).isEqualTo(42); // typical for standard test-cases

        // ... however, we could use the matchers for other code as well, e.g. for checking some user-input
        // in our production-code - which could mean, that we do not want the exceptions to be thrown, but rather
        // obtain an error-message upon failures...
        // for this case just use iceT::match rather than iceT::expectThat for all matchers, and you simply obtain
        // failures as optional error-messages rather than having to catch an exception
        auto matchResult = iceT::match(42).isEqualTo(42);
        iceT::expectThat(matchResult.has_value()).isFalse(); // if everything is ok, the result is an empty optional

        matchResult = iceT::match(42).isEqualTo(43);
        // you now get an optional with an error message telling that 43 was expected, but 42 was obtained
        iceT::expectThat(matchResult.has_value()).isTrue();
    }

    TEST(icelandExamples, matchAndExpectCanTakeExtraMessagesIfDesired) {
        // sometimes it is desired to pass a special additional error message in order to
        // identify better, why a test failed - therefore additional error messages are
        // supported
        // however, please don't overuse this feature as it can make the specs
        // more difficult to read and cumbersome to maintain

        std::string myErrMsg("something is wrong!");
        auto errorMsg =
                iceT::match(42, myErrMsg).isEqualTo(43);

        std::stringstream expectedMsg;
        expectedMsg << "isEqualTo failed: values are not equal" << std::endl <<
        "expected: " << 43 << std::endl <<
        "but obtained: " << 42 << std:: endl <<
        "something is wrong!"; // and here is our additional msg

        iceT::expectThat(errorMsg.value()).isEqualTo(expectedMsg.str());
    }

    TEST(icelandExamples, showHowToUseBooleanMatchers) {
        iceT::expectThat(1 < 17).isTrue();
        iceT::expectThat(1 > 42).isFalse();
    }

    TEST(icelandExamples, showHowToUseNullptrMatchers) {
        char* whateverNullPtr = nullptr;
        char aString[] = "otto";

        iceT::expectThat(whateverNullPtr).isNullPtr();
        iceT::expectThat(aString).isNotNullPtr();
    }

    class Whatever final {
        int value;
    public:
        Whatever(int value) : value(value) {}
        bool operator == (Whatever const& rhs) const {
            return value == rhs.value;
        }
        bool operator < (Whatever const& rhs) const {
            return value < rhs.value;
        }
    };

    TEST(icelandExamples, showEqualityMatchers) {
        // important:
        // - the call to isEqualTo (or isNotEqualTo respectively) requires that the
        // obtained and expected values support the comparison-operator == as it is used to compare them
        // - additionally the values you pass as obtained and expected have to support
        // the stream-output operator <<
        // ... it is needed in order to obtain meaningful error-messages

        iceT::expectThat(42).isEqualTo(42);
        iceT::expectThat(42).isNotEqualTo(43);

        // if the stream-output operator << is not supported, and you do not want to provide it,
        // the workaround for this situation looks like that:
        Whatever whateverVal0(17);
        Whatever whateverVal1(4);
        //iceT::expectThat(whateverVal0).isNotEqualTo(whateverVal1); // does not compile
        iceT::expectThat(whateverVal0 == whateverVal1).isFalse(); // compiles (however it gives a lousy error-message upon failure)
    }

    TEST(icelandExamples, showClosenessMatchersForNumbers) {
        // important:
        // the closeness-matcher is designed to take numbers for expected, contained and epsilon
        // it internally uses math operations as well as std::abs on the given values

        iceT::expectThat(99.5).isCloserTo(100.0).than(0.6); // works for floating-point numbers
        iceT::expectThat(33).isCloserTo(34).than(2); // works for integral numbers
        iceT::expectThat(33).isCloserTo(34.5).than(2.0f); // works for mixtures of numbers

        // we can also check, whether a number is not closer to another...
        iceT::expectThat(99.5).isNotCloserTo(100.0).than(0.4); // works for floating-point numbers
        iceT::expectThat(33).isNotCloserTo(36).than(2); // works for integral numbers
        iceT::expectThat(33).isNotCloserTo(34.5).than(1.0f); // works for mixtures of numbers

        iceT::expectThat(33).isNotCloserTo(34.5).than(1.5); // end of epsilon-range is always excluded!
    }

    TEST(icelandExamples, showSpecialMatchersForFloatingPointNumbers) {
        double aNumber = 17.0;
        iceT::expectThat(aNumber).isNotNaN();
        iceT::expectThat(aNumber).isNotInf();

        double numerator = 0.0;
        double denominator = 0.0;
        aNumber = numerator / denominator;
        iceT::expectThat(aNumber).isNaN();
        iceT::expectThat(aNumber).isNotInf(); // if it is NaN, it is not infinite

        numerator = 42.0;
        denominator = 0.0;
        aNumber = numerator / denominator;
        iceT::expectThat(aNumber).isNotNaN();
        iceT::expectThat(aNumber).isInf();
        iceT::expectThat(aNumber).isGreaterThan(0.0); // we can ask for positive infinity

        numerator = -42.0;
        denominator = 0.0;
        aNumber = numerator / denominator;
        iceT::expectThat(aNumber).isNotNaN();
        iceT::expectThat(aNumber).isInf();
        iceT::expectThat(aNumber).isLessThan(0.0); // we can ask for negative infinity
    }

    TEST(icelandExamples, showExceptionMatchers) {
        // important: you have to pass the code that should result in an exception as something "callable",
        // e.g. as a lambda, as it has to be executed inside the checker rather than upon argument evaluation!

        // simple check: expect a certain exception
        iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::out_of_range>();

        // we always check for compatible type, rather than for exact type - and out_of_range is derived from std::exception
        iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::exception>();

        // for very special cases we can also do more when checking for exceptions
        iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws(
                std::function([](std::exception const & exc) { // we pass a std::function to throws...
                    return std::string(exc.what()) == std::string("oops");
                }));
    }

    class SomeBase {};
    std::ostream& operator << (std::ostream& stream, SomeBase const& someBase ) {
        return stream << "SomeBase";
    }

    class SomeDerived : public SomeBase {};
    std::ostream& operator << (std::ostream& stream, SomeDerived const& someDerived ) {
        return stream << "SomeDerived";
    }

    class SomePrivateDerived : SomeBase {};
    std::ostream& operator << (std::ostream& stream, SomePrivateDerived const& someDerived ) {
        return stream << "SomePrivateDerived";
    }

    TEST(icelandExamples, showTypeMatchers) {
        // important: The obtained object must support the stream-output operator <<
        // ... it is needed in order to obtain meaningful error-messages

        // we can check for exact type
        iceT::expectThat(SomeDerived()).isOfExactType<SomeDerived>();
        iceT::expectThat(SomeDerived()).isNotOfExactType<SomeBase>();

        // ... and we can check for compatible type
        iceT::expectThat(SomeDerived()).isOfCompatibleTypeTo<SomeDerived>();
        iceT::expectThat(SomeDerived()).isOfCompatibleTypeTo<SomeBase>(); // only works for public inheritance...
        iceT::expectThat(SomePrivateDerived()).isNotOfCompatibleTypeTo<SomeBase>(); // ... the system can't see private base-classes
        iceT::expectThat(SomeDerived()).isNotOfCompatibleTypeTo<std::string>();
    }

    TEST(icelandExamples, showMatchersForIndexables) {
        // important: the given objects for obtained and expected have to support the
        // method .size() as well as an index-operator taking numerical indices
        iceT::expectThat(std::vector{1, 2, 3}).hasElements(std::vector{1, 2, 3});

        // works with other containers as well...
        std::array<int, 3> anArray { 4, 5, 6 };
        iceT::expectThat(anArray).hasElements(std::vector{ 4, 5, 6 });
    }

    // very simple DSL to express "an element of a vector"
    class element {
        std::vector<int> const& theVector;
        element(std::vector<int> const& vec) : theVector(vec) { }
    public:
        static element of(std::vector<int> const& vec) { return element(vec); }

        int atIndex(std::vector<int>::size_type idx) {
            return theVector.at(idx);
        }
    };

    TEST(icelandExamples, howToImplementTestHelpers) {
        // there are situations where we want some support to write well-readable specs
        // in the following an example will be shown for a small DSL that helps - although
        // this has not really anything to do with the matchers, it shall be provided as
        // an example for a good practice to help you write intuitive specs

        iceT::expectThat(element::of(std::vector<int>{ 1, 2, 3 }).atIndex(1)).isEqualTo(2);
    }

    // finally the following explains in detail, how to implement full-custom matchers if desired...
    // ...however, keep in mind not to overdo that...

    class elementOf : public iceland::matchers::CustomMatcher {
        std::vector<int> const& theVector;
        std::optional<std::vector<int>::size_type> theIdx;
        std::optional<int> theExpectedValue;
    public:
        elementOf(std::vector<int> const& vec) : theVector(vec) { }
        elementOf& atIndex(std::vector<int>::size_type idx) {
            theIdx = idx;
            return *this;
        }
        elementOf& is(int expected) {
            theExpectedValue = expected;
            return *this;
        }
        // this operator is required
        std::optional<std::string> operator()() const override {
            if (!theIdx.has_value()) { return "elementOf failed: no index given (hint: use .atIndex(...))"; }
            if (!theExpectedValue.has_value()) { return "elementOf failed: no expected value given (hint: use .is(...))"; }
            if (theIdx < 0 || theIdx >= theVector.size()) {
                return StringBuilder::str() + "elementOf failed: given index " + theIdx.value() + " is out of bounds";
            }
            auto obtained = theVector.at(theIdx.value());
            if (obtained == theExpectedValue)
                return {};
            return StringBuilder::str() + "elementOf failed: element at index " + theIdx.value() +
            " is expected to be " + theExpectedValue.value() + " but is " + obtained;
        }
    };

    TEST(icelandExamples, howToImplementFullCustomMatchers) {
        // it is overly important that any functionality we use for doing TDD is
        // solid, simple and intuitive
        // however, specific applications require quite specific kinds of tests
        // to be supported - therefore so-called "custom matcher snippets" are
        // supported
        // those snippets are usually some kind of builder that defines a little DSL
        // like the matchers themselves do - the only important thing from the point
        // of view of the matchers is, that an object with a parameterless operator()
        // (i.e. a parameterless functor) is passed which returns a std::optional<std::string>,
        // that is an empty optional if everything is ok or an error-message upon failure

        // with such an approach we are now e.g. able to write the following:
        iceT::customExpectThat(
                elementOf(std::vector<int>{ 1, 2, 3 }).atIndex(1).is(2)
                );
        // as can be seen, the built-in iceland matchers are not even used at all anymore because
        // the user-defined DSL took over everything

        // failure passes the error-message through
        try {
            iceT::customExpectThat(
                    elementOf(std::vector<int>{1, 2, 3}).atIndex(1).is(17)
            );
        } catch (IcelandMatcherFailedException const& exc) {
            iceT::expectThat(std::string(exc.what()))
            .isEqualTo(std::string("elementOf failed: element at index 1 is expected to be 17 but is 2"));
        }

        // there is also an analogous customMatch call
        auto errorMsg = iceT::customMatch(
                elementOf(std::vector<int>{ 1, 2, 3 }).atIndex(1).is(2)
                );
        iceT::expectThat(errorMsg.has_value()).isFalse();

        errorMsg = iceT::customMatch(
                elementOf(std::vector<int>{1, 2, 3}).atIndex(1).is(17)
                );
        iceT::expectThat(errorMsg.value())
                .isEqualTo(std::string("elementOf failed: element at index 1 is expected to be 17 but is 2"));
    }

    TEST(icelandExamples, thisIsTheWayToForceTheLinkerToTellYouAboutADetectedType) {
        // if, for whatever reason, you have to know which type is seen by the compiler, the
        // typeAsSeenByTheCompiler templates are of great help. They cause a linker-error and
        // thus cause the linker to tell you exactly which type it sees

        auto aVar = 42;
        using aType = const char*[];

        // the following is outcommented, because they cause a linker-error to tell us about the type...
        // works when applied to a variable
        // iceT::typeAsSeenByTheCompiler(42); // causes a linker-error which tells us exactly about the type

        // works when applied to a type
        // iceT::typeAsSeenByTheCompiler<aType>(); // causes a linker-error which tells us exactly about the type
    }

}
