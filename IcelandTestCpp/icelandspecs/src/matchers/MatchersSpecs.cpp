#include <gtest/gtest.h>
#include <matchers/Matchers.h>
#include <exceptions/IcelandMatcherFailedException.h>
#include <commontools/StringBuilder.h>
#include <stdexcept>
#include <vector>
#include <array>

using namespace iceland::exceptions;
using namespace iceland::commontools;
namespace iceT = iceland::matchers;

namespace iceland::matchers::spec {

    TEST(MatchersSpec, someExamplesHowToUseTheMatchers) {
        iceT::expectThat(true).isTrue();
        iceT::expectThat(42).isEqualTo(42);
        iceT::expectThat(42).isNotEqualTo(43);
        iceT::expectThat(42).isLessThan(43);
        iceT::expectThat(42.0).isCloserTo(42.5).than(0.6);
        iceT::expectThat((char*) nullptr).isNullPtr();
        iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::out_of_range>();
        iceT::expectThat(std::string("otto")).isOfExactType<std::string>();
        iceT::expectThat(std::string("otto")).isOfCompatibleTypeTo<std::string>();
        iceT::expectThat(std::vector{1, 2, 3}).hasElements(std::vector{1, 2, 3});
    }

    TEST(MatchersSpec, thereAreHelperTemplatesToSeeWhatTheCompilerSees) {
        auto aVar = 42;
        using aType = const char*[];

        // the following is outcommented, because they cause a linker-error to tell us about the type...
        // works when applied to a variable
        // iceT::typeAsSeenByTheCompiler(42); // causes a linker-error which tells us exactly about the type

        // works when applied to a type
        // iceT::typeAsSeenByTheCompiler<aType>(); // causes a linker-error which tells us exactly about the type
    }

    TEST(MatchersSpec, failTriggersAnUnconditionalFailOfASpec) {
        try {
            iceT::fail("bla");
        } catch (IcelandMatcherFailedException const &exc) {
            std::stringstream expectedMsg;
            expectedMsg << "failed: " << "bla";
            std::string obtainedMsg = exc.what();
            if (obtainedMsg != expectedMsg.str())
                throw IcelandMatcherFailedException("unexpected error msg from fail");
            return;
        }
        throw IcelandMatcherFailedException("fail didn't fail at all");
    }

    TEST(MatchersSpec, failCanAlsoTakeALambdaForTheMsg) {
        try {
            iceT::fail([]() { return "bla"; });
        } catch (IcelandMatcherFailedException const &exc) {
            std::stringstream expectedMsg;
            expectedMsg << "failed: " << "bla";
            std::string obtainedMsg = exc.what();
            if (obtainedMsg != expectedMsg.str())
                throw IcelandMatcherFailedException("unexpected error msg from fail");
            return;
        }
        throw IcelandMatcherFailedException("fail didn't fail at all");
    }

    TEST(MatchersSpec, isEqualToComparesObtainedAgainstExpectedForEquality) {
        {
            auto result = iceT::match(42).isEqualTo(42); // match returns an optional error string
            if (result.has_value())
                iceT::fail("isEqualTo failed");
        }
        {
            auto result = iceT::match(42).isEqualTo(43);
            std::stringstream expectedMsg;
            expectedMsg << "isEqualTo failed: values are not equal" << std::endl <<
                        "expected: 43" << std::endl <<
                        "but obtained: 42";
            std::string obtainedMsg = result.value();
            if (obtainedMsg != expectedMsg.str())
                iceT::fail("failing match did not deliver the expected error-message");
        }

        iceT::expectThat(42).isEqualTo(42); // expect throws (and internally calls the same function that match above calls)

        try {
            iceT::expectThat(42).isEqualTo(43);
        } catch (IcelandMatcherFailedException const &exc) {
            std::stringstream expectedMsg;
            expectedMsg << "isEqualTo failed: values are not equal" << std::endl <<
                        "expected: 43" << std::endl <<
                        "but obtained: 42";
            std::string obtainedMsg = exc.what();
            if (obtainedMsg != expectedMsg.str())
                throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
            return;
        }
        throw IcelandMatcherFailedException("expect didn't throw although it should have done");
    }

    TEST(MatchersSpec, matchAndAssumeCanBothTakeAdditionalErrorMessages) {
        {
            auto result = iceT::match(42, "additional error message").isEqualTo(43);
            std::stringstream expectedMsg;
            expectedMsg << "isEqualTo failed: values are not equal" << std::endl <<
                        "expected: 43" << std::endl <<
                        "but obtained: 42" << std::endl <<
                        "additional error message";
            std::string obtainedMsg = result.value();
            if (obtainedMsg != expectedMsg.str())
                iceT::fail("failing match did not deliver the expected error-message");
        }

        // no, I'm not drunk :-) We have to use a lambda here in order to allow the construct
        // return... throw ...("didn't throw at all...") at the end of this case without directly
        // returning from the whole spec and therefore skipping eventual following spec-code.
        // (there are other specs below that have multiple such calls and here it's just used for symmetry-reasons)
        []()
        {
            try {
                iceT::expectThat(42, "additional error message").isEqualTo(43);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isEqualTo failed: values are not equal" << std::endl <<
                            "expected: 43" << std::endl <<
                            "but obtained: 42" << std::endl <<
                            "additional error message";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        } (); // we directly call it...
    }

// ***** From here on all specs simply check expect(...) and do not explicitly check match(...) anymore because
// ***** the implementation of expect uses match internally and therefore this would not result in any benefit

    TEST(MatchersSpec, isNotEqualToComparesObtainedAgainstExpectedForNonEquality) {
        iceT::expectThat(42).isNotEqualTo(43);

        []() {
            try {
                iceT::expectThat(42).isNotEqualTo(42);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotEqualTo failed: values are equal" << std::endl <<
                            "42";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    // --------------------------------------------------------------------------------
    // Note: all the comparison-matchers below rely solely on the (symmetrical) existence of the "<" operator and do not need
    // any other comparison operator to be defined
    TEST(MatchersSpec, isLessThanComparesWhetherTheObtainedValueIsLessThanTheExpectedOne) {
        iceT::expectThat(42).isLessThan(43);
        iceT::expectThat(42.0).isLessThan(43.0);
        iceT::expectThat(std::string("hugo")).isLessThan(std::string("otto"));

        []() {
            try {
                iceT::expectThat(42).isLessThan(42);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isLessThan failed" << std::endl <<
                            "obtained: " << 42 << std::endl <<
                            "is not less than: " << 42;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(42).isLessThan(41);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isLessThan failed" << std::endl <<
                            "obtained: " << 42 << std::endl <<
                            "is not less than: " << 41;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isGreaterThanComparesWhetherTheObtainedValueIsGreaterThanTheExpectedOne) {
        iceT::expectThat(42).isGreaterThan(41);
        iceT::expectThat(42.0).isGreaterThan(41.0);
        iceT::expectThat(std::string("hugo")).isGreaterThan(std::string("agathe"));

        []() {
            try {
                iceT::expectThat(42).isGreaterThan(42);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isGreaterThan failed" << std::endl <<
                            "obtained: " << 42 << std::endl <<
                            "is not greater than: " << 42;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(42).isGreaterThan(43);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isGreaterThan failed" << std::endl <<
                            "obtained: " << 42 << std::endl <<
                            "is not greater than: " << 43;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isLessOrEqualThanComparesWhetherTheObtainedValueIsLessOrEqualThanTheExpectedOne) {
        iceT::expectThat(42).isLessThanOrEqualTo(43);
        iceT::expectThat(42).isLessThanOrEqualTo(42);
        iceT::expectThat(42.0).isLessThanOrEqualTo(43.0);
        iceT::expectThat(42.0).isLessThanOrEqualTo(42.0);
        iceT::expectThat(std::string("hugo")).isLessThanOrEqualTo(std::string("otto"));
        iceT::expectThat(std::string("hugo")).isLessThanOrEqualTo(std::string("hugo"));

        []() {
            try {
                iceT::expectThat(42).isLessThanOrEqualTo(41);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isLessThanOrEqualTo failed" << std::endl <<
                            "obtained: " << 42 << std::endl <<
                            "is not less or equal than: " << 41;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isGreaterOrEqualThanComparesWhetherTheObtainedValueIsGreaterOrEqualThanTheExpectedOne) {
        iceT::expectThat(42).isGreaterThanOrEqualTo(41);
        iceT::expectThat(42).isGreaterThanOrEqualTo(42);
        iceT::expectThat(42.0).isGreaterThanOrEqualTo(41.0);
        iceT::expectThat(42.0).isGreaterThanOrEqualTo(42.0);
        iceT::expectThat(std::string("hugo")).isGreaterThanOrEqualTo(std::string("agathe"));
        iceT::expectThat(std::string("hugo")).isGreaterThanOrEqualTo(std::string("hugo"));

        []() {
            try {
                iceT::expectThat(42).isGreaterThanOrEqualTo(43);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isGreaterThanOrEqualTo failed" << std::endl <<
                            "obtained: " << 42 << std::endl <<
                            "is not greater or equal than: " << 43;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    // end of comparison-matchers (see note on solely using the "<" operator)
    // --------------------------------------------------------------------------------

    TEST(MatchersSpec, isTrueChecksForBooleanBeingTrue) {
        iceT::expectThat(true).isTrue();

        []() {
            try {
                iceT::expectThat(false).isTrue();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isTrue failed";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isFalseChecksForBooleanBeingFalse) {
        iceT::expectThat(false).isFalse();

        []() {
            try {
                iceT::expectThat(true).isFalse();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isFalse failed";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNullPtrChecksForValueBeingANullPointer) {
        iceT::expectThat((char *) nullptr).isNullPtr();

        []() {
            try {
                iceT::expectThat("oops").isNullPtr();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNullPtr failed: value is not a null-pointer" << std::endl << "oops";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNotNullPtrChecksForValueNotBeingANullPointer) {
        iceT::expectThat("yeeehaaaawww").isNotNullPtr();

        []() {
            try {
                iceT::expectThat((char *) nullptr).isNotNullPtr();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotNullPtr failed: value is a null-pointer";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNaNChecksForNumbersBeingNaN) {
        iceT::expectThat(std::numeric_limits<double>::quiet_NaN()).isNaN();
        iceT::expectThat(std::numeric_limits<double>::signaling_NaN()).isNaN();
        iceT::expectThat(std::numeric_limits<float>::quiet_NaN()).isNaN();
        iceT::expectThat(std::numeric_limits<float>::signaling_NaN()).isNaN();

        []() {
            try {
                iceT::expectThat(1.0).isNaN();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNaN failed: value is not NaN" << std::endl << 1.0;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(1.0f).isNaN();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNaN failed: value is not NaN" << std::endl << 1.0;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNotNaNChecksForNumbersNotBeingNaN) {
        iceT::expectThat(1.0).isNotNaN();
        iceT::expectThat(1.0f).isNotNaN();

        []() {
            try {
                iceT::expectThat(std::numeric_limits<double>::quiet_NaN()).isNotNaN();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotNaN failed: value is NaN";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(std::numeric_limits<double>::signaling_NaN()).isNotNaN();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotNaN failed: value is NaN";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(std::numeric_limits<float>::quiet_NaN()).isNotNaN();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotNaN failed: value is NaN";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(std::numeric_limits<float>::signaling_NaN()).isNotNaN();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotNaN failed: value is NaN";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

    }

    TEST(MatchersSpec, isInfChecksForNumbersBeingInfinite) {
        iceT::expectThat(std::numeric_limits<double>::infinity()).isInf();
        iceT::expectThat(std::numeric_limits<float>::infinity()).isInf();

        []() {
            try {
                iceT::expectThat(1.0).isInf();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isInf failed: value is not infinite" << std::endl << 1.0;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(1.0f).isInf();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isInf failed: value is not infinite" << std::endl << 1.0f;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNotInfChecksForNumbersNotBeingInfinite) {
        iceT::expectThat(1.0).isNotInf();

        []() {
            try {
                iceT::expectThat(std::numeric_limits<double>::infinity()).isNotInf();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotInf failed: value is infinite";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, throwsChecksWhetherAnExceptionAssumptionIsMet) {
        iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::out_of_range>();
        // we can add a detailed check as well
        iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::out_of_range>([](auto exc){ return std::string(exc.what()) == "oops"; });

        []() {
            try {
                iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::length_error>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "throws failed - obtained exception was of unexpected type";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat([]() { throw std::out_of_range("oops"); }).throws<std::out_of_range>([](auto exc) { return std::string(exc.what()) == "fail"; });
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "throws failed - obtained compatible exception with what() being: oops" << std::endl
                            << "was not considered ok";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat([]() {}).throws<std::out_of_range>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "throws failed - no exception was thrown at all";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isCloserToChecksWhetherObtainedLiesWithinADefinedEpsilonRangeAroundExpected) {
        iceT::expectThat(42).isCloserTo(45).than(4);
        iceT::expectThat(42).isCloserTo(45).than(-4); // we always take the absolute value of the given epsilon
        iceT::expectThat(45).isCloserTo(42).than(4);
        iceT::expectThat(45).isCloserTo(42).than(-4);

        // works for all "number-types"
        iceT::expectThat(42.0).isCloserTo(42.5).than(0.6);
        iceT::expectThat(42.0).isCloserTo(42.5).than(-0.6); // we always take the absolute value of the given epsilon
        iceT::expectThat(42.5).isCloserTo(42.0).than(0.6);
        iceT::expectThat(42.5).isCloserTo(42.0).than(-0.6);

        iceT::expectThat(42.0).isCloserTo(43).than(
                2.0f); // also type-mixing is supported - as far as C++s type-system supports it

        []() {
            try {
                iceT::expectThat(42).isCloserTo(45).than(3); // the exact hit of the epsilon boundary is already considered outside!
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isCloserTo than " << 3 << " failed" << std::endl <<
                            "the absolute difference between expected: " << 45 << std::endl <<
                            "and obtained: " << 42 << std::endl <<
                            "is: " << 45 - 42;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(45).isCloserTo(42).than(3); // the exact hit of the epsilon boundary is already considered outside!
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isCloserTo than " << 3 << " failed" << std::endl <<
                            "the absolute difference between expected: " << 42 << std::endl <<
                            "and obtained: " << 45 << std::endl <<
                            "is: " << 45 - 42;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(42.0).isCloserTo(42.5).than(0.5); // the exact hit of the epsilon boundary is already considered outside!
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isCloserTo than " << 0.5 << " failed" << std::endl <<
                            "the absolute difference between expected: " << 42.5 << std::endl <<
                            "and obtained: " << 42.0 << std::endl <<
                            "is: " << 42.5 - 42.0;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(42.5).isCloserTo(42.0).than(0.5); // the exact hit of the epsilon boundary is already considered outside!
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isCloserTo than " << 0.5 << " failed" << std::endl <<
                            "the absolute difference between expected: " << 42.0 << std::endl <<
                            "and obtained: " << 42.5 << std::endl <<
                            "is: " << 42.5 - 42.0;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNotCloserToChecksWhetherObtainedLiesOutsideADefinedEpsilonRangeAroundExpected) {
        iceT::expectThat(42).isNotCloserTo(45).than(2);
        iceT::expectThat(42).isNotCloserTo(45).than(-2); // we always take the absolute value of the given epsilon
        iceT::expectThat(45).isNotCloserTo(42).than(2);
        iceT::expectThat(45).isNotCloserTo(42).than(-2);

        iceT::expectThat(42).isNotCloserTo(45).than(3); // the exact hit of the epsilon boundary is already considered outside!

        // works for all "number-types"
        iceT::expectThat(42.0).isNotCloserTo(42.5).than(0.3);
        iceT::expectThat(42.0).isNotCloserTo(42.5).than(-0.3); // we always take the absolute value of the given epsilon
        iceT::expectThat(42.5).isNotCloserTo(42.0).than(0.3);
        iceT::expectThat(42.5).isNotCloserTo(42.0).than(-0.3);

        iceT::expectThat(42.0).isNotCloserTo(43).than(
                0.5f); // also type-mixing is supported - as far as C++s type-system supports it

        []() {
            try {
                iceT::expectThat(42).isNotCloserTo(45).than(6);
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotCloserTo than " << 6 << " failed" << std::endl <<
                            "the absolute difference between expected: " << 45 << std::endl <<
                            "and obtained: " << 42 << std::endl <<
                            "is: " << 45 - 42;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        // all the other datatype-specific specs are already included in isCloserTo, as the two
        // matchers use the same function behind...
    }

    class Base {};
    class Derived : public Base {};
    inline std::ostream& operator << (std::ostream& stream, const Base& obj) { return stream << "Base"; }
    inline std::ostream& operator << (std::ostream& stream, const Derived& obj) { return stream << "Derived"; }


    TEST(MatchersSpec, isOfExactTypeChecksForValueBeingOfExactlyTheGivenType) {
        iceT::expectThat(std::string("otto")).isOfExactType<std::string>();
        iceT::expectThat(Derived()).isOfExactType<Derived>();

        []() {
            try {
                iceT::expectThat(std::string("otto")).isOfExactType<Base>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isOfExactType failed: value is not of expected type" << std::endl << "otto";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(Derived()).isOfExactType<Base>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isOfExactType failed: value is not of expected type" << std::endl << Derived();
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNotOfExactTypeChecksForValueNotBeingOfExactlyTheGivenType) {
        iceT::expectThat(std::string("otto")).isNotOfExactType<double>();
        iceT::expectThat(Derived()).isNotOfExactType<Base>();

        []() {
            try {
                iceT::expectThat(std::string("otto")).isNotOfExactType<std::string>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotOfExactType failed: value is of non-expected type" << std::endl << "otto";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(Derived()).isNotOfExactType<Derived>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotOfExactType failed: value is of non-expected type" << std::endl << Derived();
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isOfCompatibleTypeToChecksForValueBeingOfATypeCompatibleToTheExpectedOne) {
        iceT::expectThat(std::string("otto")).isOfCompatibleTypeTo<std::string>();
        iceT::expectThat(Derived()).isOfCompatibleTypeTo<Derived>();
        iceT::expectThat(Derived()).isOfCompatibleTypeTo<Base>();

        []() {
            try {
                iceT::expectThat(std::string("otto")).isOfCompatibleTypeTo<Base>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isOfCompatibleTypeTo failed: value is not of compatible type" << std::endl << "otto";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(Base()).isOfCompatibleTypeTo<Derived>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isOfCompatibleTypeTo failed: value is not of compatible type" << std::endl << Base();
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, isNotOfCompatibleTypeToChecksForValueBeingOfATypeNotCompatibleToTheExpectedOne) {
        iceT::expectThat(std::string("otto")).isNotOfCompatibleTypeTo<Base>();
        iceT::expectThat(Base()).isNotOfCompatibleTypeTo<Derived>();
        iceT::expectThat(Base()).isNotOfCompatibleTypeTo<double>();

        []() {
            try {
                iceT::expectThat(std::string("otto")).isNotOfCompatibleTypeTo<std::string>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotOfCompatibleTypeTo failed: value is of non-expected compatible type" << std::endl << "otto";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(Derived()).isNotOfCompatibleTypeTo<Base>();
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "isNotOfCompatibleTypeTo failed: value is of non-expected compatible type" << std::endl << Derived();
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    TEST(MatchersSpec, hasElementsChecksWhetherTwoIndexablesHaveEqualElements) {
        iceT::expectThat(std::vector{1, 2, 3}).hasElements(std::array{1, 2, 3});

        []() {
            try {
                iceT::expectThat(std::vector{1, 2, 3}).hasElements(std::vector{1, 2, 3, 4});
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "hasElements failed: obtained indexable is shorter than expected indexable";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(std::vector{1, 2, 3, 4}).hasElements(std::vector{1, 2, 3});
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "hasElements failed: obtained indexable is longer than expected indexable";
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();

        []() {
            try {
                iceT::expectThat(std::vector{1, 2, 3}).hasElements(std::vector{1, 2, 4});
            } catch (IcelandMatcherFailedException const &exc) {
                std::stringstream expectedMsg;
                expectedMsg << "hasElements failed: first difference found at index: " << 2;
                std::string obtainedMsg = exc.what();
                if (obtainedMsg != expectedMsg.str())
                    throw IcelandMatcherFailedException("failing expect did not deliver the expected error-message");
                return;
            }
            throw IcelandMatcherFailedException("expect didn't throw although it should have done");
        }();
    }

    // support of full-custom matchers - please use with great care and only if really needed
    // in order not to create tons of non-intuitive matchers "just for fun"

    class theValue : public CustomMatcher {
        double obtained = 0.0;
        double expected = 0.0;
    public:
        theValue(double obtained) : obtained(obtained) {}

        theValue& isTheSqrtOf(double other) {
            expected = std::sqrt(other);
            return *this;
        }

        // this operator is required
        std::optional<std::string> operator()() const override {
            if (obtained == expected)
                return {};
            return StringBuilder::str() + "theValue calculation matcher failed: expected " + expected +
            "but obtained: " + obtained;
        }
    };

    TEST(MatchersSpec, customExpectThatSupportsfullCustomMatchers) {
        iceT::customExpectThat(
                theValue(3.0).isTheSqrtOf(9.0)
                );

        []() {
            try {
                iceT::customExpectThat(
                        theValue(3.0).isTheSqrtOf(99.0)
                );
            } catch (IcelandMatcherFailedException const &exc) {
                std::string expectedMsg = StringBuilder::str() +
                                          "theValue calculation matcher failed: expected " + std::sqrt(99.0) +
                                          "but obtained: " + 3;
                iceT::expectThat(exc.what()).isEqualTo(expectedMsg);
                return;
            }
            iceT::fail("expected exception not thrown");
        }();
    }

    TEST(MatchersSpec, customMatchSupportsfullCustomMatchers) {
        auto errorMsg = iceT::customMatch(
                theValue(3.0).isTheSqrtOf(9.0)
        );
        iceT::expectThat(errorMsg.has_value()).isFalse();

        errorMsg = iceT::customMatch(
                theValue(3.0).isTheSqrtOf(99.0)
        );
        std::string expectedMsg = StringBuilder::str() +
                                  "theValue calculation matcher failed: expected " + std::sqrt(99.0) +
                                  "but obtained: " + 3;
        iceT::expectThat(errorMsg.value()).isEqualTo(expectedMsg);
    }

}