#include <gtest/gtest.h>
#include <matchers/Matchers.h>
#include <exceptions/IcelandMatcherFailedException.h>
#include <commontools/CommonUtils.h>

using namespace iceland::commontools;
namespace iceT = iceland::matchers;

namespace iceland::commontools::spec {

    class Base {};
    class Derived : public Base {};

    TEST(CommonUtilsSpec, typesAreIdenticalTellsUsWhetherTwoTypesAreIdentical) {
        iceT::expectThat(typesAreIdentical<int, int>).isTrue();
        iceT::expectThat(typesAreIdentical<int, long>).isFalse();
        iceT::expectThat(typesAreIdentical<double, double>).isTrue();
        iceT::expectThat(typesAreIdentical<float, double>).isFalse();

        iceT::expectThat(typesAreIdentical<Base, Base>).isTrue();
        iceT::expectThat(typesAreIdentical<Derived, Base>).isFalse();
        iceT::expectThat(typesAreIdentical<Base, Derived>).isFalse();

        // pointers, etc. are honored...
        iceT::expectThat(typesAreIdentical<int *, int *>).isTrue();
        iceT::expectThat(typesAreIdentical<int *, long *>).isFalse();
        iceT::expectThat(typesAreIdentical<int *, int>).isFalse();
        iceT::expectThat(typesAreIdentical<int, int *>).isFalse();
        iceT::expectThat(typesAreIdentical<Base *, Base *>).isTrue();
        iceT::expectThat(typesAreIdentical<Derived *, Base *>).isFalse();
        iceT::expectThat(typesAreIdentical<Base, Base *>).isFalse();
        iceT::expectThat(typesAreIdentical<Base *, Base>).isFalse();
    }

    TEST(CommonUtilsSpec, typeIsCompatibleToTellsUsWhetherATypeIsEitherIdenticalOrAtLeastDerivedFromTheExpectedType) {
        // fundamental types are compatible if the type is identical
        iceT::expectThat(typeIsCompatibleTo<int, int>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<int, long>).isFalse();
        iceT::expectThat(typeIsCompatibleTo<double, double>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<float, double>).isFalse();

        // non-fundamental types are compatible if the obtained type is the same or derived from the expected type
        iceT::expectThat(typeIsCompatibleTo<Base, Base>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<Derived, Base>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<Base, Derived>).isFalse();

        // pointers, etc. are honored...
        iceT::expectThat(typeIsCompatibleTo<int *, int *>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<int *, long *>).isFalse();
        iceT::expectThat(typeIsCompatibleTo<int *, int>).isFalse();
        iceT::expectThat(typeIsCompatibleTo<int, int *>).isFalse();
        iceT::expectThat(typeIsCompatibleTo<Base *, Base *>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<Derived *, Base *>).isTrue();
        iceT::expectThat(typeIsCompatibleTo<Base, Base *>).isFalse();
        iceT::expectThat(typeIsCompatibleTo<Base *, Base>).isFalse();
    }

    TEST(CommonUtilsSpec, objIsOfCompatibleTypeToTellsUsWhetherAnObjectsTypeIsEitherIdenticalOrAtLeastDerivedFromTheExpectedType) {
        // fundamental types are compatible if the type is identical
        iceT::expectThat(objIsOfCompatibleTypeTo<int>(17)).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<long>(17)).isFalse();
        iceT::expectThat(objIsOfCompatibleTypeTo<double>(17.0)).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<double>(17.0f)).isFalse();

        // non-fundamental types are compatible if the obtained type is the same or derived from the expected type
        iceT::expectThat(objIsOfCompatibleTypeTo<Base>(Base())).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<Base>(Derived())).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<Derived>(Base())).isFalse();

        // pointers, etc. are honored...
        int anInt = 17;
        long aLong = 17L;
        Base aBase;
        Derived aDerived;

        iceT::expectThat(objIsOfCompatibleTypeTo<int *>(&anInt)).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<int *>(&aLong)).isFalse();
        iceT::expectThat(objIsOfCompatibleTypeTo<int>(&anInt)).isFalse();
        iceT::expectThat(objIsOfCompatibleTypeTo<int *>(17)).isFalse();
        iceT::expectThat(objIsOfCompatibleTypeTo<Base *>(&aBase)).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<Base *>(&aDerived)).isTrue();
        iceT::expectThat(objIsOfCompatibleTypeTo<Base *>(Base())).isFalse();
        iceT::expectThat(objIsOfCompatibleTypeTo<Base>(&aBase)).isFalse();
    }

    TEST(CommonUtilsSpec, objIsOfExactTypeTellsUsWhetherAnObjectsTypeIsIdenticalToTheExpectedType) {
        iceT::expectThat(objIsOfExactType<int>(17)).isTrue();
        iceT::expectThat(objIsOfExactType<long>(17)).isFalse();
        iceT::expectThat(objIsOfExactType<double>(17.0)).isTrue();
        iceT::expectThat(objIsOfExactType<double>(17.0f)).isFalse();

        iceT::expectThat(objIsOfExactType<Base>(Base())).isTrue();
        iceT::expectThat(objIsOfExactType<Base>(Derived())).isFalse();
        iceT::expectThat(objIsOfExactType<Derived>(Base())).isFalse();

        // pointers, etc. are honored...
        int anInt = 17;
        long aLong = 17L;
        Base aBase;
        Derived aDerived;

        iceT::expectThat(objIsOfExactType<int *>(&anInt)).isTrue();
        iceT::expectThat(objIsOfExactType<int *>(&aLong)).isFalse();
        iceT::expectThat(objIsOfExactType<int>(&anInt)).isFalse();
        iceT::expectThat(objIsOfExactType<int *>(17)).isFalse();
        iceT::expectThat(objIsOfExactType<Base *>(&aBase)).isTrue();
        iceT::expectThat(objIsOfExactType<Base *>(&aDerived)).isFalse();
        iceT::expectThat(objIsOfExactType<Base *>(Base())).isFalse();
        iceT::expectThat(objIsOfExactType<Base>(&aBase)).isFalse();
    }

}