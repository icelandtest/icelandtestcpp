#include <gtest/gtest.h>
#include <matchers/Matchers.h>

#include <commontools/StringBuilder.h>
#include <exceptions/NotImplementedException.h>

namespace iceland::commontools::spec {

    namespace iceT = iceland::matchers;
    using namespace iceland::exceptions;

    TEST(StringBuilderSpec, stringBuildersProvideStringConcatenationSyntax) {
        // when
        std::string result = StringBuilder::str() +
                "the answer " + std::string("to everything is ") + 42;
        // then
        iceT::expectThat(result).isEqualTo("the answer to everything is 42");
    }
}