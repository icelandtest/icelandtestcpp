#include <gtest/gtest.h>
#include <matchers/Matchers.h>
#include <commontools/IndexableUtils.h>

#include <vector>

using namespace iceland::commontools;
namespace iceT = iceland::matchers;

namespace iceland::commontools::spec {

    TEST(IndexableUtilsSpec, idxOfFirstDifferenceToTellsUsWhereTheFirstDifferenceBetweenTwoIndexablesIs) {
        // note: in order to compile and work correctly, the template requires both indexables to implement:
        // - a size() function returning a comparable type
        // - the index-operator
        // and it also requires the stored type to implement the == operator

        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector<int>(), std::vector<int>()))
        .isEqualTo((int)IdxDifferenceFlags::NoDifference);
        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector{1, 2, 3}, std::vector{1, 2, 3}))
        .isEqualTo((int)IdxDifferenceFlags::NoDifference);
        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector{1, 2, 3, 4}, std::vector{1, 2, 3}))
        .isEqualTo((int)IdxDifferenceFlags::LhsIsLonger);
        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector{1, 2, 3}, std::vector{1, 2, 3, 4}))
        .isEqualTo((int)IdxDifferenceFlags::RhsIsLonger);
        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector{0, 2, 3}, std::vector{1, 2, 3})).isEqualTo(0);
        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector{1, 3, 3}, std::vector{1, 2, 3})).isEqualTo(1);
        iceT::expectThat(idxOfFirstDifferenceBetween(std::vector{1, 2, 4}, std::vector{1, 2, 3})).isEqualTo(2);
    }

}
